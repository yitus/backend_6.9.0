/**
 * View Models used by Spring MVC REST controllers.
 */
package yitus.web.rest.vm;
